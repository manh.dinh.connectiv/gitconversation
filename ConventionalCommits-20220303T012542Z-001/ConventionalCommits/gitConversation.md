# Conventional Commits
## Giới thiệu: Tại sao viết message commit tốt lại quan trọng

Nếu đi xem nhật ký của bất kỳ kho lưu trữ Git ngẫu nhiên nào, bạn có thể sẽ thấy các thông báo commit của nó ít nhiều là một mớ hỗn độn. Ví dụ, hãy xem ví dụ này:

<img src="./Image Pasted at 2022-2-25 14-20.png">

ở ảnh thứ nhất : message không mô tả đúng nội dung của code, k nhất quán ,người khác đọc vào không biết mình đang sửa cái gì hay là làm cái gì?




 Lịch sử commit sai:

```
$ git log --oneline -5 --author cbeams --before "Fri Mar 26 2009"

e5f4b49 Re-adding ConfigurationPostProcessorTests after its brief removal in r814. @Ignore-ing the testCglibClassesAreLoadedJustInTimeForEnhancement() method as it turns out this was one of the culprits in the recent build breakage. The classloader hacking causes subtle downstream effects, breaking unrelated tests. The test method is still useful, but should only be run on a manual basis to ensure CGLIB is not prematurely classloaded, and should not be run as part of the automated build.
2db0f12 fixed two build-breaking issues: + reverted ClassMetadataReadingVisitor to revision 794 + eliminated ConfigurationPostProcessorTests until further investigation determines why it causes downstream tests to fail (such as the seemingly unrelated ClassPathXmlApplicationContextTests)
147709f Tweaks to package-info.java files
22b25e0 Consolidated Util and MutableAnnotationUtils classes into existing AsmUtils
7f96f57 polishing
```
Vẫn là nộ dung commit đó nhưng đã đc sữa lại:


```
$ git log --oneline -5 --author pwebb --before "Sat Aug 30 2014"

5ba3db6 Fix failing CompositePropertySourceTests
84564a0 Rework @PropertySource early parsing logic
e142fd1 Add tests for ImportSelector meta-data
887815f Update docbook dependency and generate epub
ac8326d Polish mockito usage
```

- nhin vào lịch sữ trên mọi ngươi muôn xem phần nào ạ: 

- ơ hình thứ 2 : commit quá dài dòng, không có cấu trúc, 

khi chung ta viết message commit sai như vậy thì việc thiết lập lại các message đã commit lên rất là dài dòng và tốn thời gian, vì vậy chúng ta nên giảm thiểu viết messages sai , viêc viết message commit tốt biết hiệu một người có phải là developer tốt hay không.

Nếu bạn chưa suy nghĩ nhiều về điều gì tạo nên message commit  Git tuyệt vời, thì có thể bạn đã không dành nhiều thời gian để sử dụng git log và các công cụ liên quan. Có một vòng luẩn quẩn ở đây: bởi vì lịch sử commit không có cấu trúc và không nhất quán, nên người ta không dành nhiều thời gian để sử dụng hoặc sữa  nó. Và bởi vì nó không được sử dụng hoặc sữa , nó vẫn không có cấu trúc và không nhất quán.

Thành công lâu dài của một dự án phụ thuộc (trong số những thứ khác) vào khả năng bảo trì của nó và người bảo trì có ít công cụ mạnh hơn nhật ký dự án của anh ta. Bạn nên dành thời gian để học cách chăm sóc chúng đúng cách. Những gì rắc rối lúc đầu sẽ sớm trở thành thói quen, và cuối cùng là nguồn tự hào và năng suất cho tất cả những người tham gia.

Hầu hết các ngôn ngữ lập trình đều có các quy ước được thiết lập tốt về những gì tạo nên phong cách thành ngữ, tức là đặt tên, định dạng, v.v. Tất nhiên, có những biến thể về các quy ước này, nhưng hầu hết các nhà phát triển đều đồng ý rằng chọn một quy ước và tuân theo nó tốt hơn nhiều so với sự hỗn loạn xảy ra khi mọi người làm việc riêng của họ.

- công ty mình cũng có quy ước : https://gitlab.com/ConnectivCorporation/documents/guidelines/-/blob/master/git.md

Cách tiếp cận của một nhóm đối với nhật ký commit của mình không được khác biệt. Để tạo lịch sử sửa đổi tiện ích, trước tiên các nhóm nên đồng ý về quy ước thông báo cam kết xác định ít nhất ba điều sau:

 - Style. Đánh dấu cú pháp, đặt lề, ngữ pháp, viết hoa, dấu câu. Đánh vần những điều này ra, loại bỏ phỏng đoán và làm cho tất cả trở nên đơn giản nhất có thể. Kết quả cuối cùng sẽ là một nhật ký nhất quán đáng kể, không chỉ khiến bạn thích thú khi đọc mà còn thực sự được đọc thường xuyên.

- Content. Nội dung của thông báo cam kết (nếu có) nên chứa loại thông tin nào? Những gì nó không nên chứa?

- Metadata. Làm thế nào để phát hành issue tracking IDs, pull request numbers , v.v. được tham chiếu?

May mắn thay, có những quy ước được thiết lập tốt về những gì tạo nên một thông điệp cam kết Git thành ngữ. Thật vậy, nhiều người trong số họ được giả định theo cách hoạt động của một số lệnh Git. Không có gì bạn cần phải phát minh lại. Chỉ cần tuân theo bảy quy tắc dưới đây và bạn đang trên đường trở thành một người chuyên nghiệp.

## Bảy quy tắc của một thông điệp commit Git xịn xò

1. Tách chủ đề khỏi nội dung bằng một dòng trống
2. Giới hạn dòng chủ đề trong 50 ký tự
3. Viết hoa dòng chủ đề.
4. Không kết thúc dòng chủ đề bằng dấu chấm
5. Sử dụng tâm trạng mệnh lệnh trong dòng chủ đề
6. phần thân dưới 72 ký tự
7. Sử dụng phần thân để giải thích cái gì và tại sao so với như thế nào

### 1. Tách chủ đề khỏi nội dung bằng một dòng trống

Từ manpage git commit:

https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-commit.html#_discussion

Mặc dù không bắt buộc, nhưng bạn nên bắt đầu thông báo cam kết bằng một dòng ngắn (ít hơn 50 ký tự) tóm tắt thay đổi, tiếp theo là một dòng trống và sau đó là mô tả kỹ lưỡng hơn. Văn bản cho đến dòng trống đầu tiên trong thông báo cam kết được coi là tiêu đề cam kết và tiêu đề đó được sử dụng trên toàn bộ Git. Ví dụ: Git-format-patch (1) biến một cam kết thành email và nó sử dụng tiêu đề trên dòng Chủ đề và phần còn lại của cam kết trong nội dung.

Thứ nhất, không phải mọi cam kết đều yêu cầu cả chủ thể và nội dung. Đôi khi chỉ cần một dòng cũng được, đặc biệt là khi thay đổi quá đơn giản đến mức không cần thêm ngữ cảnh. Ví dụ:

[fix] list search midwifes

Nếu bạn đang thực hiện điều gì đó như thế này tại dòng lệnh, bạn có thể dễ dàng sử dụng -m option tới git commit:

``` git commit -m"Fix typo in introduction to user guide" ```

Tuy nhiên, khi cam kết có một chút giải thích và bối cảnh, bạn cần phải viết phần nội dung. Ví dụ:

``` 
Derezz the master control program

MCP turned out to be evil and had become intent on world domination.
This commit throws Tron's disc into MCP (causing its deresolution)
and turns it back into a chess game.
```


Thông điệp cam kết với các phần thân không dễ viết như vậy với tùy chọn -m. Tốt hơn hết bạn nên viết tin nhắn trong một trình soạn thảo văn bản phù hợp. Nếu bạn chưa có trình chỉnh sửa được thiết lập để sử dụng với Git tại dòng lệnh, hãy đọc phần này của Pro Git. 


### 2. Giới hạn dòng tiêu đề trong 50 ký tự

50 ký tự không phải là một giới hạn cứng, chỉ là một quy tắc chung. Giữ các dòng tiêu đề ở độ dài này đảm bảo rằng chúng có thể đọc được và buộc tác giả phải suy nghĩ trong giây lát về cách ngắn gọn nhất để giải thích những gì đang xảy ra


### 3. Viết hoa dòng chủ đề


Bắt đầu tất cả các dòng chủ đề bằng một chữ cái in hoa.

nên dùng :

```
Accelerate to 88 miles per hour
```

Thay vì: 

```
accelerate to 88 miles per hour

```

### 4. Không kết thúc dòng chủ đề bằng dấu chấm

Dấu câu ở cuối là không cần thiết trong các dòng tiêu đề. Bên cạnh đó, không gian rất quý giá khi bạn cố gắng duy trì chúng ở mức 50 ký tự trở xuống.
 vd: 

 ```
Open the pod bay doors

```

thay vì : 

```
Open the pod bay doors

```

### 5. Sử dụng  mệnh lệnh trong dòng tiêu đề

Mệnh lệnh chỉ có nghĩa là “được nói hoặc viết như thể đưa ra một mệnh lệnh hoặc chỉ dẫn”. Một vài ví dụ:

- Clean your room
- Close the door
- Take out the trash

công ty mình thì :

- feat : Khi thêm một tính năng mới
- fix : fix bug
- vv


Mệnh lệnh có thể nghe hơi thô lỗ; đó là lý do tại sao chúng tôi không thường xuyên sử dụng nó. Nhưng nó là hoàn hảo cho các dòng chủ đề cam kết Git. Một lý do cho điều này là bản thân Git sử dụng mệnh lệnh bất cứ khi nào nó tạo cam kết thay mặt bạn.

kieu này có thẻ sủ dụng:  git merge , git revert,  pull request

- Việc sử dụng mệnh lệnh chỉ quan trọng trong dòng tiêu đề. Bạn có thể nới lỏng hạn chế này khi viết phần thân.

### 6. Giới hạn phần thân ở 72 ký tự

Git không bao giờ tự động kết thúc văn bản. Khi bạn viết nội dung của một thông báo cam kết, bạn phải lưu ý đến lề phải của nó và ngắt dòng văn bản theo cách thủ công.

Khuyến nghị là thực hiện điều này ở 72 ký tự, để Git có nhiều chỗ để thụt lề văn bản trong khi vẫn giữ mọi thứ tổng thể dưới 80 ký tự

### 7. Sử dụng phần thân để giải thích cái gì và tại sao so với như thế nào

Commmit này từ  là một ví dụ về việc giải thích những gì đã thay đổi và tại sao:

```
commit eb0b56b19017ab5c16c745e6da39c53126924ed6
Author: Pieter Wuille <pieter.wuille@gmail.com>
Date:   Fri Aug 1 22:57:55 2014 +0200

   Simplify serialize.h's exception handling

   Remove the 'state' and 'exceptmask' from serialize.h's stream
   implementations, as well as related methods.

   As exceptmask always included 'failbit', and setstate was always
   called with bits = failbit, all it did was immediately raise an
   exception. Get rid of those variables, and replace the setstate
   with direct exception throwing (which also removes some dead
   code).

   As a result, good() is never reached after a failure (there are
   only 2 calls, one of which is in tests), and can just be replaced
   by !eof().

   fail(), clear(n) and exceptions() are just never called. Delete
   them.
   ```

  Hãy xem toàn bộ sự khác biệt và chỉ cần nghĩ xem tác giả đang tiết kiệm bao nhiêu thời gian cho những người đồng hành và những người cam kết trong tương lai bằng cách dành thời gian để cung cấp bối cảnh này tại đây và ngay bây giờ. Nếu anh ấy không làm vậy, nó có thể sẽ bị mất vĩnh viễn.